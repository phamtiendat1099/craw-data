# -*- coding: utf-8 -*-
import scrapy


class CFSpider(scrapy.Spider):
    name = 'cf'

    def start_requests(self):
        urls = [
            'https://codeforces.com/10years/supporters'
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        for user in response.css('a.rated-user.user-cyan::attr(href)'):
            yield scrapy.Request(response.urljoin(user.get()), callback=self.chi_tiet)
        for user in response.css('a.rated-user.user-green::attr(href)'):
            yield scrapy.Request(response.urljoin(user.get()), callback=self.chi_tiet)
        for user in response.css('a.rated-user.user-black::attr(href)'):
            yield scrapy.Request(response.urljoin(user.get()), callback=self.chi_tiet)
        for user in response.css('a.rated-user.user-gray::attr(href)'):
            yield scrapy.Request(response.urljoin(user.get()), callback=self.chi_tiet)

    def chi_tiet(self, response):
        yield{
            'link': response.url,
            'ten': response.css('div.main-info h1 a::text').get(),
            'dang_ky' : response.css('span.format-humantime::text')[1].get()
        }