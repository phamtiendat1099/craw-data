# -*- coding: utf-8 -*-
import scrapy
import mysql.connector

class Vnexpress2MysqlSpider(scrapy.Spider):
    name = 'vnexpress_mysql_2'

    def __init__(self):
        self.dem = 1
        self.connection = mysql.connector.connect(
            host = '18.141.14.102',
            user = 'test', 
            password = 'Matkhau123!@#',
            database = 'demo_crawl')
        self.cursor = self.connection.cursor()

    def start_requests(self):
        urls = [
            'https://vnexpress.net/thoi-su',
            'https://vnexpress.net/the-gioi',
            'https://vnexpress.net/kinh-doanh',
            'https://vnexpress.net/giai-tri',
            'https://vnexpress.net/the-thao',
            'https://vnexpress.net/phap-luat',
            'https://vnexpress.net/giao-duc',
            'https://vnexpress.net/suc-khoe/',
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        for post in response.css('article.list_news'):
            tieu_de = post.css('h4.title_news a::attr(title)').get()
            image_thumbnail = post.css('div.thumb_art img::attr(src)').get()
            noi_dung_tom_tat = post.css('p.description a::text').get()
            link = post.css('h4.title_news a::attr(href)').get()    

            yield scrapy.Request(
                response.urljoin(link), 
                callback=self.chi_tiet,
                cb_kwargs=dict(tieu_de = tieu_de, image_thumbnail = image_thumbnail, noi_dung_tom_tat = noi_dung_tom_tat))
      
        if self.dem < 3 :
            self.dem = self.dem + 1
            next_page = response.css('#pagination a.next::attr(href)').get()
            if next_page is not None:
                yield scrapy.Request(response.urljoin(next_page), callback=self.parse)
        else: self.dem = 1 

    def chi_tiet(self, response, tieu_de, image_thumbnail, noi_dung_tom_tat):
        if response.css('span.time.left::text').get() is not None: 
            try: 
                sql = "INSERT INTO tbl_tin_tuc VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"
                self.cursor.execute(sql,(
                    tieu_de,
                    image_thumbnail,
                    noi_dung_tom_tat,
                    response.css('span.time.left::text').get(),
                    response.css('article.content_detail').get(),
                    response.css('article.content_detail p.Normal strong::text').get(),
                    response.url.split("/")[4],
                    response.url.split("/")[2],
                ))
                self.connection.commit()
            except mysql.connector.Error as err:
                print("Something went wrong: {}".format(err))



