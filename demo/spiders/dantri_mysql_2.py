# -*- coding: utf-8 -*-
import scrapy
import mysql.connector

class DantriMysql2Spider(scrapy.Spider):
    name = 'dantri_mysql_2'

    def __init__(self):
        self.dem = 1
        self.connection = mysql.connector.connect(
            host = '18.141.14.102',
            user = 'test', 
            password = 'Matkhau123!@#',
            database = 'demo_crawl')
        self.cursor = self.connection.cursor()

    def start_requests(self):
        urls = [
            'https://dantri.com.vn/su-kien.htm',   
            'https://dantri.com.vn/xa-hoi.htm',
            'https://dantri.com.vn/the-gioi.htm',
            'https://dantri.com.vn/the-thao.htm',
            'https://dantri.com.vn/giao-duc-khuyen-hoc.htm',
            'https://dantri.com.vn/tam-long-nhan-ai.htm'
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)


    def parse(self, response):
        for post in response.css('div[id="listcheckepl"] div.clearfix'):
            tieu_de = post.css('div.mr1 h2 a::text').get()
            image_thumbnail = post.css('a img::attr(src)').get()
            noi_dung_tom_tat = post.css('div.mr1 div::text')[1].get()
            link = post.css('a::attr(href)').get()

            yield scrapy.Request(
                response.urljoin(link), 
                callback=self.chi_tiet,
                cb_kwargs=dict(tieu_de = tieu_de, image_thumbnail = image_thumbnail, noi_dung_tom_tat = noi_dung_tom_tat))
      
        if self.dem < 3 :
            self.dem = self.dem + 1
            next_page = response.css('div.clearfix div.fr a::attr(href)').get()
            if next_page is not None:
                yield scrapy.Request(response.urljoin(next_page), callback=self.parse)
        else: self.dem = 1 

    def chi_tiet(self, response, tieu_de, image_thumbnail, noi_dung_tom_tat):
        thoi_gian_dang = response.css('div.box26 span.tt-capitalize::text').get()
        if thoi_gian_dang is not None: thoi_gian_dang = thoi_gian_dang.strip()
        try: 
            sql = "INSERT INTO tbl_tin_tuc VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"
            self.cursor.execute(sql,(
                tieu_de,
                image_thumbnail,
                noi_dung_tom_tat,
                thoi_gian_dang,
                response.css('div#divNewsContent').get(),
                response.css('div#divNewsContent p strong::text').get(),
                response.url.split("/")[4],
                response.url.split("/")[2],
            ))
            self.connection.commit()
        except mysql.connector.Error as err:
            print("Something went wrong: {}".format(err))



